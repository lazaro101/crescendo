import Vue from 'vue'
import App from './App.vue'
import router from './router'
import moment from 'moment';

window.moment = moment;

Vue.config.productionTip = false

var app = new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

