import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/",
      name: "index",
      component: () => import("./components/Home")
    },
    {
      path: "/recipe",
      alias: "/recipe",
      name: "recipe",
      component: () => import("./components/Recipe")
    },
    {
      path: "/recipe/:id",
      alias: "/recipe/",
      name: "recipe-details",
      component: () => import("./components/RecipeDetails")
    },
    {
      path: "/recipe-new",
      alias: "/recipe-new",
      name: "recipe-new",
      component: () => import("./components/RecipeForm")
    },
    {
      path: "/recipe-new/:id",
      alias: "/recipe-edit",
      name: "recipe-edit",
      component: () => import("./components/RecipeForm")
    },
    {
      path: "/specials",
      alias: "/specials",
      name: "specials",
      component: () => import("./components/Specials")
    },
    {
      path: "/specials-new",
      alias: "/specials-new",
      name: "specials-new",
      component: () => import("./components/SpecialsForm")
    },
    {
      path: "/specials/:id",
      alias: "/specials-edit",
      name: "specials-edit",
      component: () => import("./components/SpecialsForm")
    },
  ]
});