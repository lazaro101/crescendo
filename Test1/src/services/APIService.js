import http from "../http-common";

class TutorialDataService {
  
  async recipes() {
    return await http.get("/recipes");
  }

  async recipeDetails(id) {
    return await http.get("/recipes/"+id);
  }

  async newRecipe(param) {
    return await http.post("/recipes", param);
  }

  async updateRecipe(param) {
    return await http.patch("/recipes/"+param.uuid, param);
  }

  async specials() {
    return await http.get("/specials");
  }

  async specialDetails(id) {
    return await http.get("/specials/"+id);
  }

  async newSpecial(param) {
    return await http.post("/specials", param);
  }

  async updateSpecial(param) {
    return await http.patch("/specials/"+param.uuid, param);
  }
  
}

export default new TutorialDataService();